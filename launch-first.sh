################# COPIA CREDENZIALI PER MAGENTO2 ###################################
if [ -d codice ] ; then
	echo "la cartella codice esiste"
	cp install_magento.sh codice/
	if [ -f codice/auth.json ] ; then
		echo "le chiavi sono presenti"
	else
	echo "inserisci le chiavi"
	touch codice/auth.json
	echo -n "Scrivi lo username: "
	read username
	echo -n "Scrivi la passsword: "
	read password
	echo '{
	   "http-basic": {
	      "repo.magento.com": {
	         "username": "'$username'",
	         "password": "'$password'"
	      }
	   }
	}' > codice/auth.json
	chmod 777 codice/auth.json
	rm codice/auth.json.sample

	exit 0
	fi

else
	echo "la cartella non esiste"
	unzip Magento-CE-2.2.0-2017-09-25-08-19-44.zip -d codice/
	cp install_magento.sh codice/
	touch codice/auth.json
	echo -n "Scrivi lo username: "
	read username
	echo -n "Scrivi la passsword: "
	read password
	echo '{
	   "http-basic": {
	      "repo.magento.com": {
	         "username": "'$username'",
	         "password": "'$password'"
	      }
	   }
	}' > codice/auth.json
	chmod 777 codice/auth.json
	rm codice/auth.json.sample

exit 0
fi

